﻿namespace Osadnicy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelWelcome = new System.Windows.Forms.Label();
            this.pictureBoxWoodcutter = new System.Windows.Forms.PictureBox();
            this.pictureBoxWarehouse = new System.Windows.Forms.PictureBox();
            this.pictureBoxSettlement = new System.Windows.Forms.PictureBox();
            this.pictureBoxFisherman = new System.Windows.Forms.PictureBox();
            this.pictureBoxBacker = new System.Windows.Forms.PictureBox();
            this.pictureBoxHounter = new System.Windows.Forms.PictureBox();
            this.progressBarWoodcutter = new System.Windows.Forms.ProgressBar();
            this.progressBarSettlement = new System.Windows.Forms.ProgressBar();
            this.progressBarFisherman = new System.Windows.Forms.ProgressBar();
            this.progressBarHounter = new System.Windows.Forms.ProgressBar();
            this.progressBarBacker = new System.Windows.Forms.ProgressBar();
            this.labelResources = new System.Windows.Forms.Label();
            this.labelTimber = new System.Windows.Forms.Label();
            this.labelFood = new System.Windows.Forms.Label();
            this.labelPopulation = new System.Windows.Forms.Label();
            this.labelTimberQuantity = new System.Windows.Forms.Label();
            this.labelFoodQuantity = new System.Windows.Forms.Label();
            this.labelPopulationQuantity = new System.Windows.Forms.Label();
            this.buttonUpgradeWoodcutter = new System.Windows.Forms.Button();
            this.buttonUpgradeSettlement = new System.Windows.Forms.Button();
            this.buttonAddWorkerFisherman = new System.Windows.Forms.Button();
            this.buttonUpgradeBacker = new System.Windows.Forms.Button();
            this.buttonUpgradeHounter = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelNameInfo = new System.Windows.Forms.Label();
            this.buttonConfirmName = new System.Windows.Forms.Button();
            this.radioButtonMale = new System.Windows.Forms.RadioButton();
            this.radioButtonFemale = new System.Windows.Forms.RadioButton();
            this.labelSex = new System.Windows.Forms.Label();
            this.buttonAddWorkerWoodcutter = new System.Windows.Forms.Button();
            this.buttonAddWorkerBacker = new System.Windows.Forms.Button();
            this.timerWoodcutter = new System.Windows.Forms.Timer(this.components);
            this.timerBacker = new System.Windows.Forms.Timer(this.components);
            this.timerSettlement = new System.Windows.Forms.Timer(this.components);
            this.buttonUpgradeFisherman = new System.Windows.Forms.Button();
            this.buttonAddWorkerHounter = new System.Windows.Forms.Button();
            this.timerFisherman = new System.Windows.Forms.Timer(this.components);
            this.timerHounter = new System.Windows.Forms.Timer(this.components);
            this.labelWoodcutter = new System.Windows.Forms.Label();
            this.labelWarehouse = new System.Windows.Forms.Label();
            this.labelSettlement = new System.Windows.Forms.Label();
            this.labelFisherman = new System.Windows.Forms.Label();
            this.labelBecker = new System.Windows.Forms.Label();
            this.labelHounter = new System.Windows.Forms.Label();
            this.labelLevelWoodcutter = new System.Windows.Forms.Label();
            this.labelLevelSettlement = new System.Windows.Forms.Label();
            this.labelLevelFisherman = new System.Windows.Forms.Label();
            this.labelLevelBecker = new System.Windows.Forms.Label();
            this.labelLevelHounter = new System.Windows.Forms.Label();
            this.labelInfoText = new System.Windows.Forms.Label();
            this.pictureBoxWood = new System.Windows.Forms.PictureBox();
            this.pictureBoxFood = new System.Windows.Forms.PictureBox();
            this.pictureBoxPopulation = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWoodcutter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSettlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFisherman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBacker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHounter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPopulation)).BeginInit();
            this.SuspendLayout();
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labelWelcome.Font = new System.Drawing.Font("Microsoft YaHei", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWelcome.Location = new System.Drawing.Point(25, 21);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(110, 35);
            this.labelWelcome.TabIndex = 0;
            this.labelWelcome.Text = "Witaj ...";
            // 
            // pictureBoxWoodcutter
            // 
            this.pictureBoxWoodcutter.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxWoodcutter.Image")));
            this.pictureBoxWoodcutter.Location = new System.Drawing.Point(32, 217);
            this.pictureBoxWoodcutter.Name = "pictureBoxWoodcutter";
            this.pictureBoxWoodcutter.Size = new System.Drawing.Size(190, 114);
            this.pictureBoxWoodcutter.TabIndex = 1;
            this.pictureBoxWoodcutter.TabStop = false;
            // 
            // pictureBoxWarehouse
            // 
            this.pictureBoxWarehouse.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxWarehouse.Image")));
            this.pictureBoxWarehouse.Location = new System.Drawing.Point(312, 245);
            this.pictureBoxWarehouse.Name = "pictureBoxWarehouse";
            this.pictureBoxWarehouse.Size = new System.Drawing.Size(190, 129);
            this.pictureBoxWarehouse.TabIndex = 2;
            this.pictureBoxWarehouse.TabStop = false;
            // 
            // pictureBoxSettlement
            // 
            this.pictureBoxSettlement.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSettlement.Image")));
            this.pictureBoxSettlement.Location = new System.Drawing.Point(571, 217);
            this.pictureBoxSettlement.Name = "pictureBoxSettlement";
            this.pictureBoxSettlement.Size = new System.Drawing.Size(190, 114);
            this.pictureBoxSettlement.TabIndex = 3;
            this.pictureBoxSettlement.TabStop = false;
            // 
            // pictureBoxFisherman
            // 
            this.pictureBoxFisherman.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFisherman.Image")));
            this.pictureBoxFisherman.Location = new System.Drawing.Point(85, 429);
            this.pictureBoxFisherman.Name = "pictureBoxFisherman";
            this.pictureBoxFisherman.Size = new System.Drawing.Size(152, 121);
            this.pictureBoxFisherman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFisherman.TabIndex = 4;
            this.pictureBoxFisherman.TabStop = false;
            // 
            // pictureBoxBacker
            // 
            this.pictureBoxBacker.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxBacker.Image")));
            this.pictureBoxBacker.Location = new System.Drawing.Point(326, 429);
            this.pictureBoxBacker.Name = "pictureBoxBacker";
            this.pictureBoxBacker.Size = new System.Drawing.Size(152, 121);
            this.pictureBoxBacker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBacker.TabIndex = 5;
            this.pictureBoxBacker.TabStop = false;
            // 
            // pictureBoxHounter
            // 
            this.pictureBoxHounter.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxHounter.Image")));
            this.pictureBoxHounter.Location = new System.Drawing.Point(561, 429);
            this.pictureBoxHounter.Name = "pictureBoxHounter";
            this.pictureBoxHounter.Size = new System.Drawing.Size(152, 121);
            this.pictureBoxHounter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHounter.TabIndex = 6;
            this.pictureBoxHounter.TabStop = false;
            // 
            // progressBarWoodcutter
            // 
            this.progressBarWoodcutter.Location = new System.Drawing.Point(32, 337);
            this.progressBarWoodcutter.Name = "progressBarWoodcutter";
            this.progressBarWoodcutter.Size = new System.Drawing.Size(190, 12);
            this.progressBarWoodcutter.TabIndex = 7;
            this.progressBarWoodcutter.Visible = false;
            // 
            // progressBarSettlement
            // 
            this.progressBarSettlement.Location = new System.Drawing.Point(568, 337);
            this.progressBarSettlement.Name = "progressBarSettlement";
            this.progressBarSettlement.Size = new System.Drawing.Size(190, 12);
            this.progressBarSettlement.TabIndex = 9;
            this.progressBarSettlement.Visible = false;
            // 
            // progressBarFisherman
            // 
            this.progressBarFisherman.Location = new System.Drawing.Point(85, 556);
            this.progressBarFisherman.Name = "progressBarFisherman";
            this.progressBarFisherman.Size = new System.Drawing.Size(152, 12);
            this.progressBarFisherman.TabIndex = 10;
            this.progressBarFisherman.Visible = false;
            // 
            // progressBarHounter
            // 
            this.progressBarHounter.Location = new System.Drawing.Point(561, 556);
            this.progressBarHounter.Name = "progressBarHounter";
            this.progressBarHounter.Size = new System.Drawing.Size(152, 12);
            this.progressBarHounter.TabIndex = 11;
            this.progressBarHounter.Visible = false;
            // 
            // progressBarBacker
            // 
            this.progressBarBacker.Location = new System.Drawing.Point(326, 556);
            this.progressBarBacker.Name = "progressBarBacker";
            this.progressBarBacker.Size = new System.Drawing.Size(152, 12);
            this.progressBarBacker.TabIndex = 12;
            this.progressBarBacker.Visible = false;
            // 
            // labelResources
            // 
            this.labelResources.AutoSize = true;
            this.labelResources.Font = new System.Drawing.Font("Microsoft YaHei", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelResources.Location = new System.Drawing.Point(598, 21);
            this.labelResources.Name = "labelResources";
            this.labelResources.Size = new System.Drawing.Size(73, 22);
            this.labelResources.TabIndex = 13;
            this.labelResources.Text = "Zasoby:";
            // 
            // labelTimber
            // 
            this.labelTimber.AutoSize = true;
            this.labelTimber.Location = new System.Drawing.Point(619, 58);
            this.labelTimber.Name = "labelTimber";
            this.labelTimber.Size = new System.Drawing.Size(49, 13);
            this.labelTimber.TabIndex = 14;
            this.labelTimber.Text = "Budulec:";
            // 
            // labelFood
            // 
            this.labelFood.AutoSize = true;
            this.labelFood.Location = new System.Drawing.Point(619, 87);
            this.labelFood.Name = "labelFood";
            this.labelFood.Size = new System.Drawing.Size(53, 13);
            this.labelFood.TabIndex = 15;
            this.labelFood.Text = "Żywność:";
            // 
            // labelPopulation
            // 
            this.labelPopulation.AutoSize = true;
            this.labelPopulation.Location = new System.Drawing.Point(619, 116);
            this.labelPopulation.Name = "labelPopulation";
            this.labelPopulation.Size = new System.Drawing.Size(57, 13);
            this.labelPopulation.TabIndex = 16;
            this.labelPopulation.Text = "Populacja:";
            // 
            // labelTimberQuantity
            // 
            this.labelTimberQuantity.AutoSize = true;
            this.labelTimberQuantity.Location = new System.Drawing.Point(684, 58);
            this.labelTimberQuantity.Name = "labelTimberQuantity";
            this.labelTimberQuantity.Size = new System.Drawing.Size(16, 13);
            this.labelTimberQuantity.TabIndex = 17;
            this.labelTimberQuantity.Text = "...";
            // 
            // labelFoodQuantity
            // 
            this.labelFoodQuantity.AutoSize = true;
            this.labelFoodQuantity.Location = new System.Drawing.Point(684, 87);
            this.labelFoodQuantity.Name = "labelFoodQuantity";
            this.labelFoodQuantity.Size = new System.Drawing.Size(16, 13);
            this.labelFoodQuantity.TabIndex = 18;
            this.labelFoodQuantity.Text = "...";
            // 
            // labelPopulationQuantity
            // 
            this.labelPopulationQuantity.AutoSize = true;
            this.labelPopulationQuantity.Location = new System.Drawing.Point(684, 116);
            this.labelPopulationQuantity.Name = "labelPopulationQuantity";
            this.labelPopulationQuantity.Size = new System.Drawing.Size(16, 13);
            this.labelPopulationQuantity.TabIndex = 19;
            this.labelPopulationQuantity.Text = "...";
            // 
            // buttonUpgradeWoodcutter
            // 
            this.buttonUpgradeWoodcutter.Location = new System.Drawing.Point(31, 161);
            this.buttonUpgradeWoodcutter.Name = "buttonUpgradeWoodcutter";
            this.buttonUpgradeWoodcutter.Size = new System.Drawing.Size(190, 22);
            this.buttonUpgradeWoodcutter.TabIndex = 20;
            this.buttonUpgradeWoodcutter.Text = "...";
            this.buttonUpgradeWoodcutter.UseVisualStyleBackColor = true;
            this.buttonUpgradeWoodcutter.Visible = false;
            this.buttonUpgradeWoodcutter.Click += new System.EventHandler(this.buttonUpgradeWoodcutter_Click);
            // 
            // buttonUpgradeSettlement
            // 
            this.buttonUpgradeSettlement.Location = new System.Drawing.Point(568, 189);
            this.buttonUpgradeSettlement.Name = "buttonUpgradeSettlement";
            this.buttonUpgradeSettlement.Size = new System.Drawing.Size(190, 22);
            this.buttonUpgradeSettlement.TabIndex = 21;
            this.buttonUpgradeSettlement.Text = " ...";
            this.buttonUpgradeSettlement.UseVisualStyleBackColor = true;
            this.buttonUpgradeSettlement.Visible = false;
            this.buttonUpgradeSettlement.Click += new System.EventHandler(this.buttonUpgradeSettlement_Click);
            // 
            // buttonAddWorkerFisherman
            // 
            this.buttonAddWorkerFisherman.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddWorkerFisherman.Location = new System.Drawing.Point(85, 403);
            this.buttonAddWorkerFisherman.Name = "buttonAddWorkerFisherman";
            this.buttonAddWorkerFisherman.Size = new System.Drawing.Size(152, 20);
            this.buttonAddWorkerFisherman.TabIndex = 22;
            this.buttonAddWorkerFisherman.Text = "...";
            this.buttonAddWorkerFisherman.UseVisualStyleBackColor = true;
            this.buttonAddWorkerFisherman.Visible = false;
            this.buttonAddWorkerFisherman.Click += new System.EventHandler(this.buttonAddWorkerFisherman_Click);
            // 
            // buttonUpgradeBacker
            // 
            this.buttonUpgradeBacker.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUpgradeBacker.Location = new System.Drawing.Point(326, 380);
            this.buttonUpgradeBacker.Name = "buttonUpgradeBacker";
            this.buttonUpgradeBacker.Size = new System.Drawing.Size(152, 20);
            this.buttonUpgradeBacker.TabIndex = 23;
            this.buttonUpgradeBacker.Text = "...";
            this.buttonUpgradeBacker.UseVisualStyleBackColor = true;
            this.buttonUpgradeBacker.Visible = false;
            this.buttonUpgradeBacker.Click += new System.EventHandler(this.buttonUpgradeBacker_Click);
            // 
            // buttonUpgradeHounter
            // 
            this.buttonUpgradeHounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUpgradeHounter.Location = new System.Drawing.Point(561, 380);
            this.buttonUpgradeHounter.Name = "buttonUpgradeHounter";
            this.buttonUpgradeHounter.Size = new System.Drawing.Size(152, 20);
            this.buttonUpgradeHounter.TabIndex = 24;
            this.buttonUpgradeHounter.Text = "...";
            this.buttonUpgradeHounter.UseVisualStyleBackColor = true;
            this.buttonUpgradeHounter.Visible = false;
            this.buttonUpgradeHounter.Click += new System.EventHandler(this.buttonUpgradeHounter_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(162, 35);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(141, 20);
            this.textBoxName.TabIndex = 25;
            // 
            // labelNameInfo
            // 
            this.labelNameInfo.AutoSize = true;
            this.labelNameInfo.Location = new System.Drawing.Point(162, 19);
            this.labelNameInfo.Name = "labelNameInfo";
            this.labelNameInfo.Size = new System.Drawing.Size(88, 13);
            this.labelNameInfo.TabIndex = 26;
            this.labelNameInfo.Text = "Podaj swoje imie:";
            // 
            // buttonConfirmName
            // 
            this.buttonConfirmName.Location = new System.Drawing.Point(162, 120);
            this.buttonConfirmName.Name = "buttonConfirmName";
            this.buttonConfirmName.Size = new System.Drawing.Size(75, 23);
            this.buttonConfirmName.TabIndex = 27;
            this.buttonConfirmName.Text = "Potwierdź";
            this.buttonConfirmName.UseVisualStyleBackColor = true;
            this.buttonConfirmName.Click += new System.EventHandler(this.buttonConfirmName_Click);
            // 
            // radioButtonMale
            // 
            this.radioButtonMale.AutoSize = true;
            this.radioButtonMale.Location = new System.Drawing.Point(165, 76);
            this.radioButtonMale.Name = "radioButtonMale";
            this.radioButtonMale.Size = new System.Drawing.Size(78, 17);
            this.radioButtonMale.TabIndex = 28;
            this.radioButtonMale.TabStop = true;
            this.radioButtonMale.Text = "Meżczyzna";
            this.radioButtonMale.UseVisualStyleBackColor = true;
            // 
            // radioButtonFemale
            // 
            this.radioButtonFemale.AutoSize = true;
            this.radioButtonFemale.Location = new System.Drawing.Point(165, 99);
            this.radioButtonFemale.Name = "radioButtonFemale";
            this.radioButtonFemale.Size = new System.Drawing.Size(61, 17);
            this.radioButtonFemale.TabIndex = 29;
            this.radioButtonFemale.TabStop = true;
            this.radioButtonFemale.Text = "Kobieta\r\n";
            this.radioButtonFemale.UseVisualStyleBackColor = true;
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(162, 58);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(92, 13);
            this.labelSex.TabIndex = 30;
            this.labelSex.Text = "Podaj swoją płeć:";
            // 
            // buttonAddWorkerWoodcutter
            // 
            this.buttonAddWorkerWoodcutter.Location = new System.Drawing.Point(31, 189);
            this.buttonAddWorkerWoodcutter.Name = "buttonAddWorkerWoodcutter";
            this.buttonAddWorkerWoodcutter.Size = new System.Drawing.Size(190, 22);
            this.buttonAddWorkerWoodcutter.TabIndex = 31;
            this.buttonAddWorkerWoodcutter.Text = "...";
            this.buttonAddWorkerWoodcutter.UseVisualStyleBackColor = true;
            this.buttonAddWorkerWoodcutter.Visible = false;
            this.buttonAddWorkerWoodcutter.Click += new System.EventHandler(this.buttonAddWorkerWoodcutter_Click);
            // 
            // buttonAddWorkerBacker
            // 
            this.buttonAddWorkerBacker.Location = new System.Drawing.Point(326, 402);
            this.buttonAddWorkerBacker.Name = "buttonAddWorkerBacker";
            this.buttonAddWorkerBacker.Size = new System.Drawing.Size(152, 20);
            this.buttonAddWorkerBacker.TabIndex = 32;
            this.buttonAddWorkerBacker.Text = "...";
            this.buttonAddWorkerBacker.UseVisualStyleBackColor = true;
            this.buttonAddWorkerBacker.Visible = false;
            this.buttonAddWorkerBacker.Click += new System.EventHandler(this.buttonAddWorkerBacker_Click);
            // 
            // timerWoodcutter
            // 
            this.timerWoodcutter.Tick += new System.EventHandler(this.timerWoodcutter_Tick);
            // 
            // timerBacker
            // 
            this.timerBacker.Tick += new System.EventHandler(this.timerBacker_Tick);
            // 
            // timerSettlement
            // 
            this.timerSettlement.Tick += new System.EventHandler(this.timerSettlement_Tick);
            // 
            // buttonUpgradeFisherman
            // 
            this.buttonUpgradeFisherman.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUpgradeFisherman.Location = new System.Drawing.Point(85, 380);
            this.buttonUpgradeFisherman.Name = "buttonUpgradeFisherman";
            this.buttonUpgradeFisherman.Size = new System.Drawing.Size(152, 20);
            this.buttonUpgradeFisherman.TabIndex = 33;
            this.buttonUpgradeFisherman.Text = "...";
            this.buttonUpgradeFisherman.UseVisualStyleBackColor = true;
            this.buttonUpgradeFisherman.Visible = false;
            this.buttonUpgradeFisherman.Click += new System.EventHandler(this.buttonUpgradeFisherman_Click);
            // 
            // buttonAddWorkerHounter
            // 
            this.buttonAddWorkerHounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddWorkerHounter.Location = new System.Drawing.Point(561, 402);
            this.buttonAddWorkerHounter.Name = "buttonAddWorkerHounter";
            this.buttonAddWorkerHounter.Size = new System.Drawing.Size(152, 20);
            this.buttonAddWorkerHounter.TabIndex = 34;
            this.buttonAddWorkerHounter.Text = "...";
            this.buttonAddWorkerHounter.UseVisualStyleBackColor = true;
            this.buttonAddWorkerHounter.Visible = false;
            this.buttonAddWorkerHounter.Click += new System.EventHandler(this.buttonAddWorkerHounter_Click);
            // 
            // timerFisherman
            // 
            this.timerFisherman.Tick += new System.EventHandler(this.timerFisherman_Tick);
            // 
            // timerHounter
            // 
            this.timerHounter.Tick += new System.EventHandler(this.timerHounter_Tick);
            // 
            // labelWoodcutter
            // 
            this.labelWoodcutter.AutoSize = true;
            this.labelWoodcutter.Location = new System.Drawing.Point(28, 217);
            this.labelWoodcutter.Name = "labelWoodcutter";
            this.labelWoodcutter.Size = new System.Drawing.Size(69, 13);
            this.labelWoodcutter.TabIndex = 35;
            this.labelWoodcutter.Text = "Chata drwala";
            // 
            // labelWarehouse
            // 
            this.labelWarehouse.AutoSize = true;
            this.labelWarehouse.Location = new System.Drawing.Point(312, 244);
            this.labelWarehouse.Name = "labelWarehouse";
            this.labelWarehouse.Size = new System.Drawing.Size(50, 13);
            this.labelWarehouse.TabIndex = 36;
            this.labelWarehouse.Text = "Magazyn";
            // 
            // labelSettlement
            // 
            this.labelSettlement.AutoSize = true;
            this.labelSettlement.Location = new System.Drawing.Point(568, 216);
            this.labelSettlement.Name = "labelSettlement";
            this.labelSettlement.Size = new System.Drawing.Size(38, 13);
            this.labelSettlement.TabIndex = 37;
            this.labelSettlement.Text = "Osada";
            // 
            // labelFisherman
            // 
            this.labelFisherman.AutoSize = true;
            this.labelFisherman.Location = new System.Drawing.Point(85, 429);
            this.labelFisherman.Name = "labelFisherman";
            this.labelFisherman.Size = new System.Drawing.Size(38, 13);
            this.labelFisherman.TabIndex = 38;
            this.labelFisherman.Text = "Rybak";
            // 
            // labelBecker
            // 
            this.labelBecker.AutoSize = true;
            this.labelBecker.Location = new System.Drawing.Point(324, 429);
            this.labelBecker.Name = "labelBecker";
            this.labelBecker.Size = new System.Drawing.Size(42, 13);
            this.labelBecker.TabIndex = 39;
            this.labelBecker.Text = "Piekarz";
            // 
            // labelHounter
            // 
            this.labelHounter.AutoSize = true;
            this.labelHounter.Location = new System.Drawing.Point(558, 425);
            this.labelHounter.Name = "labelHounter";
            this.labelHounter.Size = new System.Drawing.Size(43, 13);
            this.labelHounter.TabIndex = 40;
            this.labelHounter.Text = "Myśliwy";
            // 
            // labelLevelWoodcutter
            // 
            this.labelLevelWoodcutter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLevelWoodcutter.AutoSize = true;
            this.labelLevelWoodcutter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLevelWoodcutter.Location = new System.Drawing.Point(205, 284);
            this.labelLevelWoodcutter.Name = "labelLevelWoodcutter";
            this.labelLevelWoodcutter.Size = new System.Drawing.Size(16, 13);
            this.labelLevelWoodcutter.TabIndex = 41;
            this.labelLevelWoodcutter.Text = "...";
            // 
            // labelLevelSettlement
            // 
            this.labelLevelSettlement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLevelSettlement.AutoSize = true;
            this.labelLevelSettlement.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLevelSettlement.Location = new System.Drawing.Point(742, 284);
            this.labelLevelSettlement.Name = "labelLevelSettlement";
            this.labelLevelSettlement.Size = new System.Drawing.Size(16, 13);
            this.labelLevelSettlement.TabIndex = 42;
            this.labelLevelSettlement.Text = "...";
            // 
            // labelLevelFisherman
            // 
            this.labelLevelFisherman.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLevelFisherman.AutoSize = true;
            this.labelLevelFisherman.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLevelFisherman.Location = new System.Drawing.Point(221, 508);
            this.labelLevelFisherman.Name = "labelLevelFisherman";
            this.labelLevelFisherman.Size = new System.Drawing.Size(16, 13);
            this.labelLevelFisherman.TabIndex = 43;
            this.labelLevelFisherman.Text = "...";
            // 
            // labelLevelBecker
            // 
            this.labelLevelBecker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLevelBecker.AutoSize = true;
            this.labelLevelBecker.Location = new System.Drawing.Point(462, 508);
            this.labelLevelBecker.Name = "labelLevelBecker";
            this.labelLevelBecker.Size = new System.Drawing.Size(16, 13);
            this.labelLevelBecker.TabIndex = 44;
            this.labelLevelBecker.Text = "...";
            // 
            // labelLevelHounter
            // 
            this.labelLevelHounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLevelHounter.AutoSize = true;
            this.labelLevelHounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLevelHounter.Location = new System.Drawing.Point(697, 508);
            this.labelLevelHounter.Name = "labelLevelHounter";
            this.labelLevelHounter.Size = new System.Drawing.Size(16, 13);
            this.labelLevelHounter.TabIndex = 45;
            this.labelLevelHounter.Text = "...";
            // 
            // labelInfoText
            // 
            this.labelInfoText.AutoSize = true;
            this.labelInfoText.Location = new System.Drawing.Point(29, 56);
            this.labelInfoText.Name = "labelInfoText";
            this.labelInfoText.Size = new System.Drawing.Size(100, 13);
            this.labelInfoText.TabIndex = 47;
            this.labelInfoText.Text = "Oto Twoja osada... ";
            // 
            // pictureBoxWood
            // 
            this.pictureBoxWood.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBoxWood.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxWood.ErrorImage")));
            this.pictureBoxWood.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxWood.Image")));
            this.pictureBoxWood.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxWood.InitialImage")));
            this.pictureBoxWood.Location = new System.Drawing.Point(587, 48);
            this.pictureBoxWood.Name = "pictureBoxWood";
            this.pictureBoxWood.Size = new System.Drawing.Size(26, 23);
            this.pictureBoxWood.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxWood.TabIndex = 48;
            this.pictureBoxWood.TabStop = false;
            // 
            // pictureBoxFood
            // 
            this.pictureBoxFood.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxFood.ErrorImage")));
            this.pictureBoxFood.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFood.Image")));
            this.pictureBoxFood.Location = new System.Drawing.Point(587, 77);
            this.pictureBoxFood.Name = "pictureBoxFood";
            this.pictureBoxFood.Size = new System.Drawing.Size(26, 23);
            this.pictureBoxFood.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFood.TabIndex = 49;
            this.pictureBoxFood.TabStop = false;
            // 
            // pictureBoxPopulation
            // 
            this.pictureBoxPopulation.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPopulation.ErrorImage")));
            this.pictureBoxPopulation.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxPopulation.Image")));
            this.pictureBoxPopulation.Location = new System.Drawing.Point(587, 106);
            this.pictureBoxPopulation.Name = "pictureBoxPopulation";
            this.pictureBoxPopulation.Size = new System.Drawing.Size(26, 23);
            this.pictureBoxPopulation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPopulation.TabIndex = 50;
            this.pictureBoxPopulation.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 605);
            this.Controls.Add(this.pictureBoxPopulation);
            this.Controls.Add(this.pictureBoxFood);
            this.Controls.Add(this.pictureBoxWood);
            this.Controls.Add(this.labelInfoText);
            this.Controls.Add(this.labelLevelHounter);
            this.Controls.Add(this.labelLevelBecker);
            this.Controls.Add(this.labelLevelFisherman);
            this.Controls.Add(this.labelLevelSettlement);
            this.Controls.Add(this.labelLevelWoodcutter);
            this.Controls.Add(this.labelHounter);
            this.Controls.Add(this.labelBecker);
            this.Controls.Add(this.labelFisherman);
            this.Controls.Add(this.labelSettlement);
            this.Controls.Add(this.labelWarehouse);
            this.Controls.Add(this.labelWoodcutter);
            this.Controls.Add(this.buttonAddWorkerHounter);
            this.Controls.Add(this.buttonUpgradeFisherman);
            this.Controls.Add(this.buttonAddWorkerBacker);
            this.Controls.Add(this.buttonAddWorkerWoodcutter);
            this.Controls.Add(this.labelSex);
            this.Controls.Add(this.radioButtonFemale);
            this.Controls.Add(this.radioButtonMale);
            this.Controls.Add(this.buttonConfirmName);
            this.Controls.Add(this.labelNameInfo);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.buttonUpgradeHounter);
            this.Controls.Add(this.buttonUpgradeBacker);
            this.Controls.Add(this.buttonAddWorkerFisherman);
            this.Controls.Add(this.buttonUpgradeSettlement);
            this.Controls.Add(this.buttonUpgradeWoodcutter);
            this.Controls.Add(this.labelPopulationQuantity);
            this.Controls.Add(this.labelFoodQuantity);
            this.Controls.Add(this.labelTimberQuantity);
            this.Controls.Add(this.labelPopulation);
            this.Controls.Add(this.labelFood);
            this.Controls.Add(this.labelTimber);
            this.Controls.Add(this.labelResources);
            this.Controls.Add(this.progressBarBacker);
            this.Controls.Add(this.progressBarHounter);
            this.Controls.Add(this.progressBarFisherman);
            this.Controls.Add(this.progressBarSettlement);
            this.Controls.Add(this.progressBarWoodcutter);
            this.Controls.Add(this.pictureBoxHounter);
            this.Controls.Add(this.pictureBoxBacker);
            this.Controls.Add(this.pictureBoxFisherman);
            this.Controls.Add(this.pictureBoxSettlement);
            this.Controls.Add(this.pictureBoxWarehouse);
            this.Controls.Add(this.pictureBoxWoodcutter);
            this.Controls.Add(this.labelWelcome);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Osadnicy";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWoodcutter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSettlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFisherman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBacker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHounter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPopulation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.PictureBox pictureBoxWoodcutter;
        private System.Windows.Forms.PictureBox pictureBoxWarehouse;
        private System.Windows.Forms.PictureBox pictureBoxSettlement;
        private System.Windows.Forms.PictureBox pictureBoxFisherman;
        private System.Windows.Forms.PictureBox pictureBoxBacker;
        private System.Windows.Forms.PictureBox pictureBoxHounter;
        private System.Windows.Forms.ProgressBar progressBarWoodcutter;
        private System.Windows.Forms.ProgressBar progressBarSettlement;
        private System.Windows.Forms.ProgressBar progressBarFisherman;
        private System.Windows.Forms.ProgressBar progressBarHounter;
        private System.Windows.Forms.ProgressBar progressBarBacker;
        private System.Windows.Forms.Label labelResources;
        private System.Windows.Forms.Label labelTimber;
        private System.Windows.Forms.Label labelFood;
        private System.Windows.Forms.Label labelPopulation;
        private System.Windows.Forms.Label labelTimberQuantity;
        private System.Windows.Forms.Label labelFoodQuantity;
        private System.Windows.Forms.Label labelPopulationQuantity;
        private System.Windows.Forms.Button buttonUpgradeWoodcutter;
        private System.Windows.Forms.Button buttonUpgradeSettlement;
        private System.Windows.Forms.Button buttonAddWorkerFisherman;
        private System.Windows.Forms.Button buttonUpgradeBacker;
        private System.Windows.Forms.Button buttonUpgradeHounter;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelNameInfo;
        private System.Windows.Forms.Button buttonConfirmName;
        private System.Windows.Forms.RadioButton radioButtonMale;
        private System.Windows.Forms.RadioButton radioButtonFemale;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Button buttonAddWorkerWoodcutter;
        private System.Windows.Forms.Button buttonAddWorkerBacker;
        private System.Windows.Forms.Timer timerWoodcutter;
        private System.Windows.Forms.Timer timerBacker;
        private System.Windows.Forms.Timer timerSettlement;
        private System.Windows.Forms.Button buttonUpgradeFisherman;
        private System.Windows.Forms.Button buttonAddWorkerHounter;
        private System.Windows.Forms.Timer timerFisherman;
        private System.Windows.Forms.Timer timerHounter;
        private System.Windows.Forms.Label labelWoodcutter;
        private System.Windows.Forms.Label labelWarehouse;
        private System.Windows.Forms.Label labelSettlement;
        private System.Windows.Forms.Label labelFisherman;
        private System.Windows.Forms.Label labelBecker;
        private System.Windows.Forms.Label labelHounter;
        private System.Windows.Forms.Label labelLevelWoodcutter;
        private System.Windows.Forms.Label labelLevelSettlement;
        private System.Windows.Forms.Label labelLevelFisherman;
        private System.Windows.Forms.Label labelLevelBecker;
        private System.Windows.Forms.Label labelLevelHounter;
        private System.Windows.Forms.Label labelInfoText;
        private System.Windows.Forms.PictureBox pictureBoxWood;
        private System.Windows.Forms.PictureBox pictureBoxFood;
        private System.Windows.Forms.PictureBox pictureBoxPopulation;
    }
}

