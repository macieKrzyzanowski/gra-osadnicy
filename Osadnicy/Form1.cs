﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Osadnicy
{

    public partial class Form1 : Form
    {
        /// <summary>
        /// Tablica przechowuje liste wszystkich przysickow do dodawnaie pracownika 
        /// 0 - buttonAddWorkerWoodcutter,
        /// 1 - buttonAddWorkerFisherman,
        /// 2 - buttonAddWorkerBacker,
        /// 3 - buttonAddWorkerHounter
        /// </summary>
        Button[] listOfButtonsAddWorker;

        /// <summary>
        /// Tablica przechowuje liste wszystkich przysickow do zwiększania poziomu budynku  
        /// 0 - buttonUpgradeWoodcutter,
        /// 1 - buttonUpgradeFisherman,
        /// 2 - buttonUpgradeBacker,
        /// 3 - buttonUpgradeHounter
        /// 4 - buttonUpgradeSettlement
        /// </summary>
        Button[] listOfButtonsUpgrade;

        /// <summary>
        /// tablica przechowuje liste wszystkich progres barow 
        /// 0 - progressBarWoodcutter,
        /// 1 - progressBarFisherman,
        /// 2 - progressBarBacker,
        /// 3 - progressBarHounter,
        /// 4 - progressBarSettlement
        /// </summary>
        ProgressBar[] listOfProgrestBar;

        /// <summary>
        /// Tablica przechowuje liste wszystkich napisow odnoszaczych sie do lvl budynku
        /// 0 - labelLevelWoodcutter,
        /// 1 - labelLevelFisherman,
        /// 2 - labelLevelBecker,
        /// 3 - labelLevelHounter, 
        /// 4 - labelLevelSettlement
        /// </summary>
        Label[] listOfLabelLevel;

        public Form1()
        {
            InitializeComponent();
            listOfButtonsAddWorker = new Button[] //inicjalizacjia tablicy przyciskow do dodawania pracowników
            {
                buttonAddWorkerWoodcutter,
                buttonAddWorkerFisherman,
                buttonAddWorkerBacker,
                buttonAddWorkerHounter
            };
            listOfButtonsUpgrade = new Button[] //inicjalozacja tablicy przyciskow do podnoszenia poziomu budynku 
            {
                buttonUpgradeWoodcutter,
                buttonUpgradeFisherman,
                buttonUpgradeBacker,
                buttonUpgradeHounter,
                buttonUpgradeSettlement
            };
            listOfProgrestBar = new ProgressBar[] //inicjalizacjia tablicy progres bar'ow
            {
                progressBarWoodcutter,
                progressBarFisherman,
                progressBarBacker,
                progressBarHounter,
                progressBarSettlement
            };
            listOfLabelLevel = new Label[] //inicjalizacja tablicy napisow o levelach
            {
                labelLevelWoodcutter,
                labelLevelFisherman,
                labelLevelBecker,
                labelLevelHounter,
                labelLevelSettlement
            };

        }

        /// <summary>
        /// Klasa zawiera inforamcje o budynkach 
        /// </summary>
        public class Building
        {
            public int level; //lvl budynku
            public int costLevelUpp; //koszt uleszenia
            public int lowerEfficiency; //dolny poziom produktu, jaki moze budynek wyprodukowac
            public int upperEfficiency; //gorny poziom produktu, jaki moze budynek wyprodukowac
            public int worker; //ilosc pracowanikow w budynku
            public int costWorker; //koszt zwiekszenia ilosci pracownikow 

            //konstruktor ustawiajacy poczatkowe parametry
            public Building()
            {
                level = 1;
                costLevelUpp = 200;
                lowerEfficiency = 5;
                upperEfficiency = 10;
                costWorker = 1;
                worker = 1;
            }
            //Metoda losuje z podanego przedzialu zasob, ktory budynek wyprodukuje 
            virtual public int Produce()
            {
                Random random_number = new Random();
                return random_number.Next(lowerEfficiency, upperEfficiency);
            }
            //Metoda podnosi poziom budyunku 
            virtual public void levelUpp()
            {
                level++;
                costLevelUpp += level * 20;
                lowerEfficiency *= 2;
                upperEfficiency *= 2;
            }
            //Metoda dodaje nowych pracownikow
            public void moreWorker()
            {
                worker++;
                costWorker += worker * 2;
            }
        }

        /// <summary>
        /// Klasa pochodna, dziedziczaca elementy po klasie Building
        /// </summary>
        public class Woodcutter : Building { }

        /// <summary>
        /// Klasa pochodna, dziedziczaca elementy po klasie Building
        /// </summary>
        public class Backer : Building
        {
            //konsturktor ustawiajacy parametry obiektu
            public Backer(int myCostLevelUpp = 200, int myLowerEfficiency = 5, int myUpperEfficiency = 10, int myWorker = 1)
            {
                costLevelUpp = myCostLevelUpp;
                lowerEfficiency = myLowerEfficiency;
                upperEfficiency = myUpperEfficiency;
                worker = myWorker;
            }
        }

        /// <summary>
        /// Klasa pochodna, dziedziczaca elementy po klasie Building
        /// </summary>
        public class Fisherman : Building
        {
            //konsturktor ustawiajacy parametry obiektu
            public Fisherman(int myCostLevelUpp = 200, int myLowerEfficiency = 5, int myUpperEfficiency = 10, int myWorker = 1)
            {
                costLevelUpp = myCostLevelUpp;
                lowerEfficiency = myLowerEfficiency;
                upperEfficiency = myUpperEfficiency;
                worker = myWorker;
            }
        }

        /// <summary>
        /// Klasa pochodna, dziedziczaca elementy po klasie Building
        /// </summary>
        public class Hounter : Building
        {
            //konsturktor ustawiajacy parametry obiektu
            public Hounter(int myCostLevelUpp = 200, int myLowerEfficiency = 5, int myUpperEfficiency = 10, int myWorker = 1)
            {
                costLevelUpp = myCostLevelUpp;
                lowerEfficiency = myLowerEfficiency;
                upperEfficiency = myUpperEfficiency;
                worker = myWorker;
            }
        }

        /// <summary>
        /// Klasa pochodna, dziedziczaca elementy po klasie Building
        /// </summary>
        public class Settlement : Building
        {
            int populationGrowth = 2; //przyrost popoulacji, ktory wytwarza osada

            //konstruktor ustawiajacy parametry obiektu 
            public Settlement(int myCostLevelUpp = 150)
            {
                costLevelUpp = myCostLevelUpp;
            }
            override public int Produce() //nadpisanie metody, ktora zwieksza teraz ilosc populacji 
            {
                return populationGrowth;
            }
            override public void levelUpp() //zwiekszenie lvl. budynku
            {
                level++;
                costLevelUpp += level * 20;
                populationGrowth *= 2;
            }
        }

        /// <summary>
        /// Tablica przechowujące informacje o budynkach
        /// building[0] - woodcutter
        /// building[1] - fisherman
        /// building[2] - backer
        /// building[3] - hounter
        /// building[4] - settlement
        /// </summary>
        Building[] building = new Building[]
        {
            new Woodcutter(),
            new Fisherman(80,2,5,3), //inicjalizacja nowego budynku z parametrami innymi niz domyslne
            new Backer(60,1,3,1),
            new Hounter(100,3,8,2),
            new Settlement()
        };

        /*Zmienne przechowuja informacje o posiadanych zasobach*/
        int timber = 200;
        int food = 100;
        int population = 1;

        /// <summary>
        /// Funckja akualizuje wszystkie informacje znajdujace sie w grze.
        /// </summary>
        void updateInformation()
        {
            //aktualizacja informacji o koszcie ulepszenia 
            for (int i = 0; i < listOfButtonsUpgrade.Length; i++)
            {
                //Warunek sprawdza ktorego budynku dotyczy aktualizacja danych. 
                if(i < 4)
                {
                    listOfButtonsUpgrade[i].Text = "Ulepsz(koszt: " + building[i].costLevelUpp.ToString() + " budulca)";
                }
                else //Jezeli osady, koszt ulepszenia liczony jest w zywności
                {
                    listOfButtonsUpgrade[i].Text = "Ulepsz(koszt: " + building[i].costLevelUpp.ToString() + " żywności)";
                }
            }
                

            //aktualizacja informacji o koszcie dodania pracowanika
            for (int i = 0; i < listOfButtonsAddWorker.Length; i++)
                listOfButtonsAddWorker[i].Text = "Dodaj pracowanika(koszt: " + building[i].costWorker.ToString() + " popul.)";

            //aktualizacja informacji o poziomie budynkow ilosci pracownikow i ilosci produkcji
            for (int i = 0; i < listOfLabelLevel.Length; i++)
                listOfLabelLevel[i].Text = "Level: " + building[i].level.ToString() +
                                            "\nPracownicy: " + building[i].worker.ToString() + 
                                            "\nProdukcja: " + building[i].lowerEfficiency.ToString() + "-" + building[i].upperEfficiency.ToString();

            //aktualizacja informacji o posiadanych surowcach
            labelFoodQuantity.Text = food.ToString();
            labelPopulationQuantity.Text = population.ToString();
            labelTimberQuantity.Text = timber.ToString();
        }

        /// <summary>
        /// Funckja aktualizuje informacje o progress bar
        /// W argumacie funckji podajemy progress bar ktory chemy zaktualizowac, budynek ktorego dotyczy, oraz zasobu ktory budynek zwiększa
        /// </summary>
        void checkProgressBar(ProgressBar myProgressBar, Building myBuilding,ref int resource)
        {
            if (myProgressBar.Value < 100) //jezeli procesbar nie osiagnal maksymalnej wartosci 
            {
                myProgressBar.Increment(myBuilding.worker); //inkrementuje procesbar, o raztosc zalezna od ilosci pracownikow budynku
                                                            //jezeli pracownikow jest wiecej, procesbar uzupelnia sie szybciej
            }
            else
            {
                resource += myBuilding.Produce(); //dodanie wylosowanej ilosci zasobu
                updateInformation(); //aktualizacje informacji w grze
                myProgressBar.Value = 0; //wyzerowanie progres bar'u
            }

        }

        /// <summary>
        /// Funckja obsluguje zdarzenie upeszenia budynku podanego w argumencie funckji
        /// </summary>
        /// <param name="myBuilding"></param>
        void buildingLevelUp(Building myBuilding)
        {
            if (timber >= myBuilding.costLevelUpp) //sprawdzanie czy mamy wystaczajaca ilosc zasobow
            {
                timber -= myBuilding.costLevelUpp; //jesli tak zmniejszay ilosc zasobow o koszt budowy 
                myBuilding.levelUpp(); //podnosimy poziom budynku
                updateInformation(); //aktualizujemy informacje 
            }
            else
            {
                MessageBox.Show("Nie masz wystarczającej ilości budulca"); //jesli nie, wyswietlamy stotsowna informacje 
            }

        }
        /// <summary>
        /// Funckja obsluguje zdarzenie zwiększenia ilości pracowników budynku podanego w argumencie funckji
        /// </summary>
        /// <param name="myBuilding"></param>
        void buildingAddWorker(Building myBuilding)
        {
            if (population >= myBuilding.costWorker) //sprawdzanie czy mamy wystaczajaca ilosc populacji
            {
                population -= myBuilding.costWorker; //jesli tak zmniejszay ilosc zasobow populacji o koszt budowy 
                myBuilding.moreWorker(); //podnosimy ilosc pracownikow 
                updateInformation(); //aktualizujemy informacje 
            }
            else
            {
                MessageBox.Show("Nie masz wystarczającej populacji"); //jesli nie wyswietlamy sototwona inforamacje
            }

        }

        /// <summary>
        /// Przycisk obsluguje pobranie nazwy uzytkowanika
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConfirmName_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text.Length == 0) //sprawdzenie czy uzytkownik podal imie
            {
                MessageBox.Show("Podaj imię!");
            }
            else if(!radioButtonFemale.Checked && !radioButtonMale.Checked) //sprawdzenie czy uzytkownik podal plec
            {
                MessageBox.Show("Podaj płeć!");
            }
            else
            {
                //Dostosowanie powitania do plci gracza
                if (radioButtonMale.Checked) //jezeli zazanczyl radioButtonMale
                    labelWelcome.Text = "Witaj Sir " + textBoxName.Text + "!";
                else //jezeli zaznaczyl radioButtonFemale
                    labelWelcome.Text = "Witaj Lady " + textBoxName.Text + "!";

                labelNameInfo.Visible = textBoxName.Visible = buttonConfirmName.Visible = false;
                labelSex.Visible = radioButtonMale.Visible = radioButtonFemale.Visible = false;
                updateInformation(); //zaktualizowanie informacji o zasobach i kosztach
                /*wystartowanie timerow ospowieadajacych za prace prcessBar'ow*/
                timerBacker.Start();
                timerSettlement.Start();
                timerWoodcutter.Start();
                timerFisherman.Start();
                timerHounter.Start();
                labelInfoText.Text = "Oto Twoja osada. " +
                    "\nPosiadasz 3 podstawowe zasoby: budulec, żywność i populacje." +
                    "\nKażdy z zasobów umożliwia rozbudwe budynku. " +
                    "\nZwiekszenie ilości pracowników powoduje szybszą prace budynku" +
                    "\nZwększenie poziomu budynku, zwiększa efektywność pracy\n" +
                    "Zarządzaj swoją osoadą mądrze !";


                for (int i = 0; i < listOfButtonsUpgrade.Length; i++)
                    listOfButtonsUpgrade[i].Visible = true;

                for (int i = 0; i < listOfButtonsAddWorker.Length; i++)
                    listOfButtonsAddWorker[i].Visible = true;

                for (int i = 0; i < listOfProgrestBar.Length; i++)
                    listOfProgrestBar[i].Visible = true;

            }
        }

        /// <summary>
        /// Funkcja odpwiada za prace timera w trakcie ktorej sprawdzany jest stan procesBar'u Woodcutter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerWoodcutter_Tick(object sender, EventArgs e)
        {
            checkProgressBar(progressBarWoodcutter, building[0],ref timber);
        }
        /// <summary>
        /// Funkcja odpwiada za prace timera w trakcie ktorej sprawdzany jest stan procesBar'u Fisherman
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerFisherman_Tick(object sender, EventArgs e)
        {
            checkProgressBar(progressBarFisherman, building[1],ref food);
        }
        /// <summary>
        /// Funkcja odpwiada za prace timera w trakcie ktorej sprawdzany jest stan procesBar'u Backer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerBacker_Tick(object sender, EventArgs e)
        {
            checkProgressBar(progressBarBacker, building[2],ref food);
        }
        /// <summary>
        /// Funkcja odpwiada za prace timera w trakcie ktorej sprawdzany jest stan procesBar'u Hounter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerHounter_Tick(object sender, EventArgs e)
        {
            checkProgressBar(progressBarHounter, building[3],ref food);
        }
        /// <summary>
        /// Funkcja odpwiada za prace timera w trakcie ktorej sprawdzany jest stan procesBar'u Settlement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerSettlement_Tick(object sender, EventArgs e)
        {
            if (progressBarSettlement.Value < 100) //jezeli procesbar nie osiagnal maksymalnej wartosci 
            {
                progressBarSettlement.Increment(2); //inkrementacja progressBar'u 
            }
            else
            {
                population += building[4].Produce(); //dodanie odpwoieidniej ilosci populacji 
                updateInformation(); //aktual. inforamcji w grze
                progressBarSettlement.Value = 0; //wyzerowanie progresbar'u
            }
        }

        /// <summary>
        /// Przycisk ulepsza budynek Woodcutter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpgradeWoodcutter_Click(object sender, EventArgs e)
        {
            buildingLevelUp(building[0]);     
        }

        /// <summary>
        /// Przycisk dodaje nowego pracownika do budynku Woodcutter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddWorkerWoodcutter_Click(object sender, EventArgs e)
        {
            buildingAddWorker(building[0]);
        }
        /// <summary>
        /// Przycisk ulepsza budynek Fisherman
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpgradeFisherman_Click(object sender, EventArgs e)
        {
            buildingLevelUp(building[1]);
        }

        /// <summary>
        /// Przycisk dodaje nowego pracownika do budynku Fisherman
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddWorkerFisherman_Click(object sender, EventArgs e)
        {
            buildingAddWorker(building[1]);
        }

        /// <summary>
        /// Przycisk ulepsza budynek Backer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpgradeBacker_Click(object sender, EventArgs e)
        {
            buildingLevelUp(building[2]);
        }

        /// <summary>
        /// Przycisk dodaje nowego pracownika do budynku Backer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddWorkerBacker_Click(object sender, EventArgs e)
        {
            buildingAddWorker(building[2]);
        }
        /// <summary>
        /// Przycisk ulepsza budynek Hounter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpgradeHounter_Click(object sender, EventArgs e)
        {
            buildingLevelUp(building[3]);
        }

        /// <summary>
        /// Przycisk dodaje nowego pracownika do budynku Hounter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddWorkerHounter_Click(object sender, EventArgs e)
        {
            buildingAddWorker(building[3]);
        }
        /// <summary>
        /// Przycisk ulepsza budynek Settlement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpgradeSettlement_Click(object sender, EventArgs e)
        {
            if (food >= building[4].costLevelUpp) //sprawdzanie czy mamy wystaczajaca ilosc zasobow
            {
                food -= building[4].costLevelUpp; //jesli tak zmniejszay ilosc zasobow o koszt budowy 
                building[4].levelUpp(); //podnosimy poziom budynku
                updateInformation(); //aktualizujemy informacje 
            }
            else
            {
                MessageBox.Show("Nie masz wystarczającej ilości żywności"); //jesli nie, wyswietlamy stotsowna informacje 
            }
        }

    }
}
